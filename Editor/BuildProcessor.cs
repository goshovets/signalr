using System.Collections.Generic;
using System.IO;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Ejaw.Editor
{
    public class BuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        
        public void OnPreprocessBuild(BuildReport report)
        {
            CopyLinks();
        }

        private static void CopyLinks()
        {
            var req = UnityEditor.PackageManager.Client.List(true);
            while (!req.IsCompleted)
            {
                continue;
            }
     
            var linkPaths = new List<(string, string)>();
            var collection = req.Result;
            foreach (var p in collection)
            {
                CollectLinks(linkPaths, p.resolvedPath, p.resolvedPath);
            }
     
            if (linkPaths.Count > 0)
            {
                foreach (var (absPath, relPath) in linkPaths)
                {
                    var destPath = $"{Application.dataPath}/PackageLinks/{relPath}";
               
                    Debug.Log($"Copying {absPath} to {destPath}...");
     
                    Directory.CreateDirectory(Path.GetDirectoryName(destPath)!);
                    File.Copy(absPath, destPath, true);
                }
            }
            else
            {
                Debug.Log("No package links found.");
            }
        }
     
        private static void CollectLinks(List<(string, string)> linkPaths, string rootPath, string path)
        {
            // check files
            var files = Directory.GetFiles(path, "link.xml");
            foreach (var file in files)
            {
                linkPaths.Add((file, file.Replace(rootPath, "")));
            }
     
            // check directories
            var directories = Directory.GetDirectories(path);
            foreach (var dir in directories)
            {
                CollectLinks(linkPaths, rootPath, dir);
            }
        }
    }
}
