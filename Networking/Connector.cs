using System;
using Microsoft.AspNetCore.SignalR.Client;
using UnityEngine;

namespace Ejaw.Networking
{
    public class Connector
    {
        public HubConnection Connection { get; }
        public bool EnableLogging { get; set; }

        public Connector(string url, bool enableLogging = false)
        {
            EnableLogging = enableLogging;
            Connection = new HubConnectionBuilder()
                .WithUrl(url)
                .Build();
        }

        public Connector(string url, bool enableLogging = false, TimeSpan[] reconnectDelays = default)
        {
            EnableLogging = enableLogging;
            if (reconnectDelays == default)
            {
                Connection = new HubConnectionBuilder()
                    .WithUrl(url)
                    .WithAutomaticReconnect()
                    .Build();
            }
            else
            {
                Connection = new HubConnectionBuilder()
                    .WithUrl(url)
                    .WithAutomaticReconnect(reconnectDelays)
                    .Build();
            }
        }

        public void Disconnect()
        {
            Connection.StopAsync();

            if (EnableLogging)
            {
                Debug.Log("Connection canceled");
            }
        }

        public async void Connect()
        {
            try
            {
                await Connection.StartAsync();

                if (EnableLogging)
                {
                    Debug.Log("Connection started");
                }
            }
            catch (Exception ex)
            {
                if (EnableLogging)
                {
                    Debug.LogError(ex);
                }
            }
        }

        public async void Send(string methodName, object[] args)
        {
            try
            {
                await Connection.InvokeCoreAsync(methodName, typeof(object), args);

                if (EnableLogging)
                {
                    var sentMessage = "";
                    foreach (var arg in args)
                    {
                        sentMessage += $"\n {arg}";
                    }

                    Debug.Log($"Sent: {sentMessage}");
                }
            }
            catch (Exception ex)
            {
                if (EnableLogging)
                {
                    Debug.LogError(ex);
                }
            }
        }

        public void RegisterHandler(string methodName, Action handler)
        {
            Connection.On(methodName, handler);
        }
        
        public void RegisterHandler<T>(string methodName, Action<T> handler)
        {
            Connection.On(methodName, handler);
        }
        
        public void RegisterHandler<T1, T2>(string methodName, Action<T1, T2> handler)
        {
            Connection.On(methodName, handler);
        }
        
        public void RegisterHandler<T1, T2, T3>(string methodName, Action<T1, T2, T3> handler)
        {
            Connection.On(methodName, handler);
        }
        
        public void RemoveHandler(string methodName)
        {
            Connection.Remove(methodName);
        }
    }
}